# A Terraform plan to start a k8s cluster with Atomic

resource "aws_security_group" "k8s" {
  name = "k8s"
  description = "Kubernetes traffic"

  ingress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }

}

resource "template_file" "kubelet" {
    template = "${path.module}/scripts/kubelet"

    vars {
        master_ip = "${aws_instance.master.private_ip}"
    }

    depends_on = ["aws_instance.master"]
}

resource "template_file" "config" {
    template = "${path.module}/scripts/config"

    vars {
        master_ip = "${aws_instance.master.private_ip}"
    }

    depends_on = ["aws_instance.master"]
}

resource "template_file" "flanneld" {
    template = "${path.module}/scripts/flanneld"

    vars {
        master_ip = "${aws_instance.master.private_ip}"
    }

    depends_on = ["aws_instance.master"]
}

resource "aws_instance" "master" {

    ami = "${lookup(var.master_ami, var.region)}"
    instance_type = "${var.master_instance_type}"
    key_name = "k8s"
    security_groups = ["${aws_security_group.k8s.name}"]

    connection {
        type = "ssh"
        user = "centos"
        private_key = "${file("${var.key_path}${var.key_name}")}"
    }

    provisioner "file" {
        source = "${path.module}/scripts/etcd"
        destination = "/home/centos/etcd"
    }

    provisioner "file" {
        source = "${path.module}/scripts/apiserver"
        destination = "/home/centos/apiserver"
    }

    provisioner "file" {
        source = "${path.module}/scripts/flannel-config.json"
        destination = "/home/centos/flannel-config.json"
    }
    
    provisioner "file" {
        source = "${path.module}/scripts/scheduler"
        destination = "/home/centos/scheduler"
    }

    provisioner "file" {
        source = "${path.module}/scripts/controller-manager"
        destination = "/home/centos/controller-manager"
    }
    
    provisioner "remote-exec" {
        scripts = [
            "${path.module}/scripts/master.sh",
        ]
    }

    tags {
        Name = "master"
    }
}

resource "aws_instance" "worker" {

    depends_on = ["aws_instance.master"]

    ami = "${lookup(var.ami, var.region)}"
    instance_type = "${var.instance_type}"
    key_name = "k8s"
    count = "${var.servers}"
    security_groups = ["${aws_security_group.k8s.name}"]
    
    connection {
        type = "ssh"
        user = "centos"
        private_key = "${file("${var.key_path}${var.key_name}")}"
    }

    provisioner "file" {
        source = "${path.module}/scripts/proxy"
        destination = "/home/centos/proxy"
    }

    provisioner "remote-exec" {
        inline = [
            "cat <<'EOF' > /home/centos/config\n${template_file.config.rendered}\nEOF",
            "cat <<'EOF' > /home/centos/kubelet\n${template_file.kubelet.rendered}\nEOF",
            "cat <<'EOF' > /home/centos/flanneld\n${template_file.flanneld.rendered}\nEOF"
        ]
    }

    provisioner "remote-exec" {
        scripts = [
            "${path.module}/scripts/worker.sh",
        ]
    }

    tags {
        Name = "worker-${count.index}"
    }
}
