variable "ami" {
    default = {
        eu-west-1 = "ami-03a2ad65"
    }
}

variable "master_ami" {
    default = {
        eu-west-1 = "ami-03a2ad65"
    }
}

variable "key_name" {
    default = "k8s.pem"
    description = "SSH key name in your AWS account for AWS instances."
}

variable "key_path" {
    default = ""
    description = "Path to the private key specified by key_name."
}

variable "region" {
    default = "eu-west-1"
    description = "The region of AWS, for AMI lookups."
}

variable "servers" {
    default = "2"
    description = "The number of k8s workers to launch."
}

variable "instance_type" {
    default = "t2.micro"
    description = "The instance type to launch."
}

variable "master_instance_type" {
    default = "t2.micro"
    description = "The instance type to launch."
}
