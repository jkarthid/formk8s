#!/bin/bash

sudo mkdir /etc/etcd
sudo mkdir /etc/kubernetes
sudo cp /home/centos/etcd /etc/etcd/etcd.conf
sudo cp /home/centos/config /etc/kubernetes/config
sudo cp /home/centos/apiserver /etc/kubernetes/apiserver

sudo cp /home/centos/etcd /etc/init.d/etcd
sudo cp /home/centos/apiserver /etc/init.d/kube-apiserver
sudo cp /home/centos/scheduler /etc/init.d/kube-scheduler
sudo cp /home/centos/controller-manager /etc/init.d/kube-controller-manager


for s in etcd kube-apiserver kube-controller-manager kube-scheduler; do
    sudo service $s restart
    sudo chkconfig $s on
    sudo service $s status
done

sudo etcdctl set coreos.com/network/config < /home/centos/flannel-config.json

sudo service flanneld restart
sudo chkconfig flanneld on
sudo service flanneld status

sudo reboot
