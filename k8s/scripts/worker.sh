#!/bin/bash

sudo mkdir /etc/kubernetes
sudo mkdir /etc/etcd
sudo cp /home/centos/config /etc/kubernetes/config
sudo cp /home/centos/kubelet /etc/kubernetes/kubelet
sudo cp /home/centos/proxy /etc/kubernetes/proxy
sudo cp /home/centos/flanneld /etc/sysconfig/flanneld

sudo cp /home/centos/config /etc/init.d/config
sudo cp /home/centos/kubelet /etc/init.d/kubelet
sudo cp /home/centos/proxy /etc/init.d/kube-proxy
sudo cp /home/centos/flanneld /etc/init.d/flanneld

for s in kube-proxy kubelet flanneld; do
    sudo service $s restart 
    sudo chkconfig $s on
    sudo service  $s status
done

sudo reboot
